const express = require('express');

const router = new express.Router();

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/courses', (req, res) => {
    res.render('courses');
});

router.get('/contact', (req, res) => {
    res.render('contact');
});

router.get('/thanks', (req, res) => {
    res.render('thanks');
});

module.exports = router;

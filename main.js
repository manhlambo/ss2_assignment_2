const express = require('express');
const path = require('path');
const homeRouter = require('./controllers/homeController.js');
const errRouter = require('./controllers/errorController.js');

const app = express();

const publicDirPath = path.join(__dirname, '/public');

app.set('view engine', 'ejs');
app.use(express.static(publicDirPath));
app.use(homeRouter);
app.use(errRouter);

// start the server
const port = 3000;
app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});
